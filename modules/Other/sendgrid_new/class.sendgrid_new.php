<?php

/**
 * HostBill - Client Management, Billing & Support System for WebHosts
 * Copyright (c) 2010-2018 HostBill All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied
 * only  in  accordance  with  the  terms  of such  license and with the
 * inclusion of the above copyright notice.  This software  or any other
 * copies thereof may not be provided or otherwise made available to any
 * other person.  No title to and  ownership of the  software is  hereby
 * transferred.
 *
 * You may not reverse  engineer, decompile, defeat  license  encryption
 * mechanisms, or  disassemble this software product or software product
 * license.  HostBill  may terminate this license if you don't comply with any
 * of the terms and conditions set forth in our end user  license agreement
 * (EULA).  In such event,  licensee  agrees to return licensor  or destroy
 * all copies of software  upon termination of the  license.
 *
 */
//require_once MAINDIR . 'includes' . DS . 'modules' . DS . 'Other' . DS . 'sendgrid_new' . DS . 'vendor' . DS . 'autoload.php';


class sendgrid_new extends OtherModule implements Observer, Sortable
{
    use \Components\Traits\LoggerTrait;

    /**
     * @var Sorter
     */
    var $sorter;
    /**
     * Module version, visible in admin portal.
     * @var string
     */
    protected $version = '1.2019-06-04';

    /**
     * Module name, visible in admin portal.
     * @var string
     */
    protected $modname = 'SendGrid';

    /**
     * Module description, visible in admin portal
     * @var string
     */
    protected $description = 'Export customer data to SendGrid marketing automation software';

    /**
     * Connected instance of PDO object
     * @var PDO
     */
    protected $db;

    protected $_repository = 'plugin_sendgrid';


    /**
     * Information about the module contained in the database
     * @var array
     */
    protected $info = array(
        'haveadmin' => true,
        'isobserver' => true,
        'extras_menu' => true
    );

    /**
     * HostBill will auto-propagate values for this configuration before calling any method from module
     * @var array
     */
    protected $configuration = [
        'API key' => array(
            'value' => '',
            'type' => 'input',
            'description' => ''
        ),
        'Auto-subscribe new customer signups' => array(
            'value' => '0',
            'type' => 'check',
            'description' => ''
        ),
        'Field for opt-in signup' => array(
            'value' => '',
            'type' => 'input',
            'description' => ''
        ),
        'Remove the sendgrid member' => array(
            'value' => '0',
            'type' => 'check',
            'description' => 'Remove the sendgrid member along with the HB profile'
        ),
        'Export customer contacts' => array(
            'value' => '0',
            'type' => 'check',
            'description' => ''
        )

    ];

    protected $timeout = 30;
    protected $verify_ssl = true;

    /**
     * Instance from SendGridAPI
     * @var SendGridAPI
     */
    protected $sendgrid;

    protected $count = 1000;

    /**
     * sendgrid constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->sorter = new Sorter("sendgrid", $this->getAvailableFields(), $_REQUEST);




    }

    /**
     * The method starts when the module is installed
     */

    public function install() {

        $this->db->exec("CREATE TABLE IF NOT EXISTS `hb_sendgrid_clients` (
          `client_id` int(11) NOT NULL,
          `sendgrid_client_id` VARCHAR (60),
          `sendgrid_list_id` int(11),
          `date_created` datetime NOT NULL,
          `date_updated` datetime NOT NULL,
          PRIMARY KEY (`client_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

    }


    public function request($endpoint, $method, $data = NULL, $header = ''){
        $curl = curl_init();


        if($header)
            $http_header = array_merge(['authorization: Bearer '.$this->configuration['API key']['value']], $header);
        else
            $http_header = ['authorization: Bearer '.$this->configuration['API key']['value']];

        curl_setopt_array($curl, [
            CURLOPT_URL => $endpoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $data ? json_encode($data) : '{}',
            CURLOPT_HTTPHEADER => $http_header
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);


        if ($err) {
            return ['error' => $err];
        } else {
            return $response;
        }
    }

    /**
     * Add a new customer to a store
     * @param Client $client
     * @return bool|mixed|null
     */
    public function addCustomer($client){

        $email = strtolower($client['email']);

        $data[] = [
            "email" => $email,
            "first_name"    => $client['firstname'],
            "last_name"     => $client['lastname'],
        ];

        $header = ["content-type: application/json"];


        $result = $this->request("https://api.sendgrid.com/v3/contactdb/recipients", "POST", $data, $header);


        $result = $this->checkResult($result);

        return $result;
    }


    /**
     * Delete a SendGrid customer from a store
     * @param $client
     * @param $list
     * @return bool|mixed|null
     */

    public function deleteCustomer($sendgrid_client_id){


        $result = $this->request("https://api.sendgrid.com/v3/contactdb/recipients/$sendgrid_client_id", "DELETE");

        $result = $this->checkResult($result);

        return $result;
    }

    /**
     * Update a SendGrid customer.
     * @param $client
     * @param $list
     * @return bool|mixed|null
     *
     */
    public function editCustomer($client){
        $client = $client->export();



        $data[] = [
            'email'             => $client['email'],
            'first_name'        => $client['firstname'],
            'last_name'         => $client['lastname']
        ];
        $header = ["content-type: application/json"];


        $result = $this->request("https://api.sendgrid.com/v3/contactdb/recipients", "PATH", $data, $header);
        $result = $this->checkResult($result);

        return $result;
    }


    /**
     * Adds a list member to SendGrid
     * @param $params
     * @param $params
     */
    public function after_clientadded($params){
        if ($this->configuration['Auto-subscribe new customer signups']['value'] == 0) {
            $field = $this->configuration['Field for opt-in signup']['value'];
            $field = $params[$field];
            if ($field == '') {
                return;
            }
        }

        $this->addMember($params);
    }



    public function after_clientedit($params){
        if (Components\HBQueue\HBQueue::isEnabled()){
            if ($this->configuration['Auto-subscribe new customer signups']['value'] == 0) {
                Components\HBQueue\HBQueue::dispatch([$this,"changeSubscribeStatus"],$params);
            }


        }else{
            $this->logger()->error('HBQueue is disabled -> after client edit');
        }
    }

    public function after_contactadd($params){
        if ($this->configuration['Export customer contacts']['value'] != 0){
            $params['id'] = $params['contact_id'];
            $this->after_clientadded($params);
        }
    }

    /**
     * Removes a list member from SendGrid
     * @param $params
     */
    public function before_clientdelete($params){

        $client = Client::load($params['id']);
        if ($this->configuration['Remove the sendgrid member']['value'] != 0){
            $query = $this->db->prepare("SELECT sendgrid_client_id FROM hb_sendgrid_clients WHERE client_id = :client_id");
            $query->execute(array(
                'client_id' => $params['id']
            ));
            $sendgrid_client = $query->fetchAll(PDO::FETCH_ASSOC);
            $query->closeCursor();




            $this->deleteCustomer($sendgrid_client['sendgrid_client_id']);
            $query = $this->db->prepare("DELETE FROM hb_sendgrid_clients WHERE client_id = :client_id");
            $query->execute(array(
                    'client_id' => $params['id']
            ));
            $query->closeCursor();

        }
    }


    /**
     * Get information about all lists
     * @return bool|mixed
     */
    public function getLists(){


        $lists = $this->request("https://api.sendgrid.com/v3/contactdb/lists", "GET");
        $result = $this->checkResult($lists);

        return $result;
    }

    public function createLists($name){

        $data = ['name' => $name];
        $header = ['content-type: application/json'];
        $lists = $this->request("https://api.sendgrid.com/v3/contactdb/lists", "POST", $data, $header);
        $result = $this->checkResult($lists);

        return $result;
    }

    public function addClientToList($client_id, $list_id){

        $data = [$client_id];
        $header= ["content-type: application/json"];
        $result = $this->request('https://api.sendgrid.com/v3/contactdb/lists/'.$list_id.'/recipients', "POST", $data, $header);

        if(!isset($result['error']))
            return true;
        return false;

    }



    /**
     * Get information about members in a list
     * @param $list_id
     * @return bool|mixed
     */
    public function getMembers($list_id){



        $result = $this->request("https://api.sendgrid.com/v3/contactdb/lists/$list_id/recipients", "GET");

        $result = $this->checkResult($result);

        return $result;
    }



    /**
     * Checks whether there are HTML tags in the string
     * @param $string
     * @return bool
     */
    public function isHTML($string){
        if($string != strip_tags($string)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * It checks the response from the API and displays any errors
     * @param $result
     * @return null
     */
    private function checkResult($result){
        if (isset($result['error'])){

                Engine::addError('SendGrid: '.$result['error']['errors']['message']);

                return null;

        }else{
            return json_decode($result, 1);
        }
    }

    /**
     * This method adds the client to the SendGrid list
     * @param $params
     * @param bool $status
     * @param $interests
     * @param $list_id
     */
    public function addMember($params){

        $list =  HBConfig::getConfig("SendGridList");

        $customer = $this->addCustomer($params);

        if($list)
            $this->addClientToList($customer['persisted_recipients'][0], $list);

        $query = $this->db->prepare("INSERT INTO hb_sendgrid_clients (client_id, sendgrid_client_id, sendgrid_list_id, date_created, date_updated)
                    VALUES (:client_id, :sendgrid_client_id, :sendgrid_list_id, :datec, :dateu);");
        $data = array(
            'client_id' => $params['id'],
            'sendgrid_client_id' => $customer['persisted_recipients'][0],
            'sendgrid_list_id' => $list,
            'datec' => date('Y-m-d H:i:s'),
            'dateu' => date('Y-m-d H:i:s')
        );

        $query->execute($data);
        $query->closeCursor();
    }



    /**
     * This method changes the subscription status of a member in the  list
     * @param $params
     */
    public function changeSubscribeStatus($params){

        $field = $this->configuration['Field for opt-in signup']['value'];
        foreach ($params['new'][$field] as $value){
            $query = $this->db->prepare("SELECT client_id FROM hb_sendgrid_clients WHERE client_id = :client_id");
            $query->execute(array(
                'client_id' => $params['previous']['id']
            ));
            $subscribe_status = $query->fetch(PDO::FETCH_ASSOC);
            $query->closeCursor();

            if ($params['previous'][$field][0] == '0' && $value == 1){
                if (empty($subscribe_status['client_id'])){

                    $this->addMember($params['new']);
                }

            }elseif ($params['previous'][$field][0] != '0' && $value != 1){
                if (!empty($subscribe_status['client_id'])){
                    $client = Client::load($params['previous']['id']);

                    $this->changeStatus($params['previous']['id']);
                }
            }
        }
    }

    public function changeStatus($client_id){
        $query = $this->db->prepare("SELECT sendgrid_client_id FROM hb_sendgrid_clients WHERE client_id = :client_id");
        $query->execute(array(
            'client_id' => $client_id
        ));
        $sengrid_id = $query->fetch(PDO::FETCH_ASSOC);

        $query->closeCursor();

        $this->deleteCustomer($sengrid_id['sendgrid_client_id']);

        $query = $this->db->prepare("DELETE FROM hb_sendgrid_clients WHERE client_id = :client_id");
        $query->execute(array(
            'client_id' => $client_id
        ));
        $query->closeCursor();
    }


    /**
     * This method returns all HB users who do not belong to any list
     * @return array
     */
    public function getNotexportedClients(){

        if ($this->configuration['Auto-subscribe new customer signups']['value'] != '0'){
            $field_id = false;
            $consent = 'auto';
        }else{
            $query = $this->db->prepare("SELECT id
            FROM hb_client_fields
            WHERE code = :code");
            $query->execute(array(
                'code' => $this->configuration['Field for opt-in signup']['value']
            ));
            $field_id = $query->fetch(PDO::FETCH_ASSOC);
            $query->closeCursor();

            $field_id = $field_id['id'];
            $consent = 'no';
        }

        $contacts = '';

        if ($this->configuration['Export customer contacts']['value'] == 0){
            $contacts = 'AND cd.parent_id = 0';
        }

        $query = $this->db->prepare("SELECT cd.id, cd.parent_id, cd.firstname, cd.lastname, ca.email, cd.companyname, :consent AS 'consent'
        FROM hb_client_details cd
        JOIN hb_client_access ca ON (cd.id = ca.id)
                        WHERE cd.id NOT IN (
                            SELECT client_id as id FROM hb_sendgrid_clients 
                            
                        )".$contacts." ". $this->sorter->sort('AND'));
        $query->execute(array(
            'consent' => $consent
        ));
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        $query->closeCursor();

        if (!empty($field_id)){
            $query = $this->db->prepare("SELECT cd.id, cd.parent_id, cd.firstname, cd.lastname, ca.email, cd.companyname, IF(cv.value IS NULL or cv.value = '' or cv.value = '0', 'no', 'yes') AS 'consent'
                FROM hb_client_details cd
                JOIN hb_client_access ca ON (cd.id = ca.id)
                JOIN hb_client_fields_values cv ON (cd.id = cv.client_id)
                WHERE cd.id NOT IN (
                            SELECT client_id as id FROM hb_sendgrid_clients 
                            
                        )
                AND cv.field_id = :field_id  ".$contacts."  ". $this->sorter->sort('AND'));
            $query->execute(array(
                'field_id' => $field_id

            ));
            $with_field = $query->fetchAll(PDO::FETCH_ASSOC);
            $query->closeCursor();

            foreach ($results as $key => $result){
                foreach ($with_field as $id => $value){
                    if ($result['id'] == $value['id']){
                        $results[$key] = $with_field[$id];
                    }
                }
            }
        }
        return $results;
    }

    /**
     * This method exports all selected clients to the  list
     * @param $clients_id
     * @param $consents
     * @param array $merge
     * @param $interests
     * @param $list
     */
    public function addNotexportedClientsToList($clients_id, $consents){


        foreach ($clients_id as $key => $value){
            $query = $this->db->prepare("SELECT client_id FROM hb_sendgrid_clients WHERE client_id = :client_id");
            $query->execute(array(
                'client_id' => $value['id']
            ));
            $result = $query->fetch(PDO::FETCH_ASSOC);
            $query->closeCursor();

            if (empty($result['client_id'])){
                $status = false;
                if ($consents[$key] == 'yes' || $consents[$key] == 'auto'){
                    $status = 'subscribed';
                }elseif ($consents[$key] == 'no'){
                    $status = 'unsubscribed';
                }
                $client = Client::load($value['id']);
                $this->addMember($client->export());
            }
        }
    }



    /**
     * SORTABLE
     */

    public function getCurrentFilter() {
        return $this->sorter->getCurrentFilter();
    }



    public function getAvailableFields() {
        return array('cd.id', 'cd.firstname', 'cd.lastname', 'ca.email', 'cd.companyname', 'consent');
    }

    public function getTotalPages($id=false) {

        $perpage = $this->sorter->getPerPage();
        try{
            $q = $this->db->prepare("SELECT COUNT(id) as total 
                FROM hb_client_details
                WHERE id NOT IN (
                    SELECT client_id as id FROM hb_sendgrid_clients 
                    
                ) ");


            $total = $q->fetch(PDO::FETCH_ASSOC);
            $q->closeCursor();
            $total = $total['total'];
            $return = array(
                'perpage' => $perpage,
                'totalpages' => ceil($total / $perpage),
                'sorterrecords' => $total,
                'sorterpage' => $this->sorter->getCurrentPage(),
            );
        }catch (Exception $e){
            $this->addError($e->getMessage());
        }

        return $return;
    }

    public function getCustomer($client_id){


        $query = $this->db->prepare("SELECT sendgrid_client_id FROM hb_sendgrid_clients WHERE client_id = :client_id");
        $query->execute(array(
            'client_id' => $client_id
        ));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();


        return $result;
    }


    public function massExport($clients, $quantity, $which){
        $this->addInfo("Queue for exporting clients {$which} out of {$quantity}");


        $query = $this->db->prepare("INSERT INTO hb_sendgrid_clients (client_id, sendgrid_client_id, date_created, date_updated)
                    VALUES (:client_id, :sendgrid_client_id, :datec, :dateu);");

        foreach ($clients as $client){
            $client_load = Client::load($client['id']);

            $result = $this->addCustomer($client_load);

            $client = $client_load->export();

            $datasql = [
                'client_id' => $client['id'],
                'sendgrid_client_id' => $result['persisted_recipients'][0],
                'datec' => date('Y-m-d H:i:s'),
                'dateu' => date('Y-m-d H:i:s')
            ];

            $query->execute($datasql);

        }
        $query->closeCursor();



    }

    public function getAllClients(){
        $part_size = 500;



        $contacts = '';

        if ($this->configuration['Export customer contacts']['value'] == 0){
            $contacts = 'AND cd.parent_id = 0';

        }

        if ($this->configuration['Auto-subscribe new customer signups']['value'] != '0'){
            $query = $this->db->prepare("SELECT cd.id FROM hb_client_details cd
                WHERE cd.id NOT IN (
                    SELECT client_id as id FROM hb_sendgrid_clients 
                    
                ) ".$contacts." ");
            $query->execute();
            $clients = $query->fetchAll(PDO::FETCH_ASSOC);
            $query->closeCursor();

        }else{
            $code = $this->configuration['Field for opt-in signup']['value'];

            if (empty($code)){
                $this->addInfo('Automatic subscription is disabled and the field "Field for opt-in signup" is not defined');
                $this->addInfo('No customer has been exported');
                return false;
            }

            $query = $this->db->prepare("SELECT id FROM hb_client_fields WHERE code = :code");
            $query->execute(array(
                'code' => $code
            ));
            $code_id = $query->fetch(PDO::FETCH_ASSOC);
            $query->closeCursor();
            $code_id = $code_id['id'];
            if (empty($code_id)){
                $this->addError('The field defined in "Field for opt-in signup" does not exist');
                return false;
            }

            $query = $this->db->prepare("SELECT cd.id FROM hb_client_details cd
                JOIN hb_client_fields_values cf ON (cd.id = cf.client_id)
                WHERE cd.id NOT IN (
                    SELECT client_id as id FROM hb_sendgrid_clients 
                    
                )
                AND cf.field_id = :code_id
                AND cf.value != '0'
                AND cf.value != '' ".$contacts." ");
            $query->execute(array(

                'code_id' => $code_id
            ));
            $clients = $query->fetchAll(PDO::FETCH_ASSOC);
            $query->closeCursor();
        }

        if (empty($clients)){
            $this->addInfo('No customers to export');
            return false;
        }

        $array_parts = array_chunk($clients, $part_size);


        if (Components\HBQueue\HBQueue::isEnabled()){
            $token = array();
            $quantity = count($array_parts);
            foreach ($array_parts as $key => $part){
                if ($key == 0){
                    $token[$key] = Components\HBQueue\HBQueue::dispatch([$this,"massExport"],$part, $quantity, $key+1);
                }else{
                    $token[$key] = Components\HBQueue\HBQueue::builder()
                        ->dispatch([$this, 'massExport'], $part, $quantity, $key+1)
                        ->parentToken($token[$key-1])
                        ->commit();
                }
            }

        }else{
            $this->logger()->error('HBQueue is disabled -> massExport');
            $this->addError('HBQueue is disabled');
            return false;
        }

        return true;
    }

    private function translate($name, $language){
        if (strchr($name,'{$lang.')){

            preg_match('/{\$lang\.\w+}/', $name, $output_array);

            $translate = $output_array[0];
            $translate = str_replace('{$lang.', '', $translate);
            $translate = str_replace('}', '', $translate);

            $lang = new Language($language, 'user');
            $lang->loadDictionary();
            $translate = $lang->translate($translate);

            $result = str_replace($output_array[0], $translate, $name);
        }else{
            $result = $name;
        }

        return $result;
    }

    private function getClient($client_id) {
        if (empty($client_id))
            return false;

        $query = $this->db->prepare("SELECT sendgrid_client_id FROM hb_sendgrid_clients 
          WHERE client_id = :client_id AND sendgrid_client_id IS NOT NULL");
        $query->execute(array('client_id' => $client_id));
        $hash = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();

        if (!$hash)
            return false;

        return $hash;
    }

    public function getCurrentOrder() {
        return $this->sorter->getCurrentOrder();
    }


}