<form action="?cmd={$modulename}&action=export" method="post">
    <div style="padding-bottom: 13px;">
        <label style="margin-bottom: 0px;">Customers listed below are not exported by this plugin to configured SendGrid list</label>
        <button id="btn-export" class="btn btn-success btn-sm right" name="save" value="save">export selected</button>
    </div>
    <table cellspacing="0" cellpadding="3" border="0" width="100%" class="table glike hover">
            <thead>
                <tr>
                    <th width="20"><input id="checkall" type="checkbox" name="export[all]"></th>
                    <th width="150">Check all/checkbox</th>
                    <th>Id</th>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Email</th>
                    <th>Company name</th>
                    <th>Consent</th>
                    {if $contact.value}<th>Parent client</th>{/if}
                </tr>
            </thead>
            <tbody>
                {if $clients}
                    {foreach from=$clients item=client}
                        <tr>
                            <td><input class="checkbox-export" type="checkbox" name="export[{$client.id}]"></td>
                            <td></td>
                            <td><a href="?cmd=clients&action=show&id={$client.id}">{$client.id}</a></td>
                            <td><a href="?cmd=clients&action=show&id={$client.id}">{$client.firstname}</a></td>
                            <td><a href="?cmd=clients&action=show&id={$client.id}">{$client.lastname}</a></td>
                            <td>{$client.email}</td>
                            <td>{$client.companyname}</td>
                            <td>{$client.consent}<input type="hidden" name="consent[{$client.id}]" value="{$client.consent}"></td>
                            {if $contact.value}<td>{if $client.parent_id == 0}-{else}<a href="?cmd=clients&action=show&id={$client.parent_id}">{$client.parent_id}</a>{/if}</td>{/if}
                        </tr>
                    {/foreach}
                {/if}
            </tbody>
            {securitytoken}
    </table>
    {if !$clients}
        <div class="text-center">
            <label style="padding: 10px;">Nothing to display</label>
        </div>
    {/if}
    {if $totalpages}
        <center class="blu paginercontainer" >
            <strong>{$lang.Page} </strong>
            {section name=foo loop=$totalpages}
                <a href='?cmd={$modulename}&action=export&page={$smarty.section.foo.iteration-1}&security_token={$security_token}' class="npaginer
                               {if $smarty.section.foo.iteration-1==$currentpage}
                                   currentpage
                               {/if}"
                >{$smarty.section.foo.iteration}</a>

            {/section}
        </center>
        <script> $('.paginercontainer', 'div.slide:visible').infinitepages();
            FilterModal.bindsorter('{$orderby.orderby}', '{$orderby.type}');</script>
    {/if}
</form>
