<div class="newhorizontalnav bordered" id="newshelf" style="background-color: white">
    <div class="list-1">
        <ul>
            <li class="list-1 {if $tab == 'configuration'}active picked{/if}">
                <a href="?cmd={$modulename}">{if $lang.configuration}{$lang.configuration}{else}Configuration{/if}</a>
            </li>
            <li class="list-1 {if $tab == 'export'}active picked{/if}">
                {if $lists_error}
                    <a style="color: #c2c2c2">{if $lang.bulkexport}{$lang.bulkexport}{else}Bulk Export{/if}</a>
                {else}
                    <a href="?cmd={$modulename}&action=export" >{if $lang.bulkexport}{$lang.bulkexport}{else}Bulk Export{/if}</a>
                {/if}
            </li>
            <li class="list-1 {if $tab == 'mass_export'}active picked{/if}">
                <a href="?cmd={$modulename}&action=mass_export" >{if $lang.massexport}{$lang.massexport}{else}Mass Export{/if}</a>
            </li>
        </ul>
    </div>
</div>
<div class="nicerblu">
    <div class="newtab">
        {if $tab == 'configuration'}
            {include file="tpl/configuration.tpl"}
        {/if}
        {if $tab == 'export'}
            {include file="tpl/export.tpl"}
        {/if}
        {if $tab == 'mass_export'}
            {include file="tpl/mass_export.tpl"}
        {/if}
    </div>
</div>
<script type="text/javascript" src="{$system_url}includes/modules/Other/{$modulename}/admin/js/script.js"></script>
