<link rel="stylesheet" type="text/css" href="{$system_url}includes/modules/Other/{$modulename}/admin/css/style.css">
<div class="content">
    <div class="row">
        <div class="col-sm-12 config_forms">
            {if $lists_error}
                <label>{$lists_error.detail}</label>
            {else}
                <form action="" method="post" enctype="multipart/form-data" class="form-inline">
                    <div class="row row-margin">
                        <div class="form-group col-sm-12">
                            <label for="default_lists">Default list for new clients</label>
                            <select id="default_lists" class="form-control" name="default_lists">
                                {foreach from=$lists item=list}
                                    <option value="{$list.id}" {if $list.default == 1}selected{/if}>{$list.name}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div id="loadingDiv" class="loader center-block">test</div>
                    </div>

                    <div class="row row-margin">
                        <div class="form-group col-sm-5">
                            <label for="default_lists">New list</label>
                            <input type="text" name="new_list_name" value="" placeholder="add new list name">
                        </div>
                    </div>
                    <div class="clear"></div>



                    {if $auto_subscribe}
                        <div class="row row-margin">
                            <div class="form-group">
                                <input id="auto_sub" type="hidden" value="true">
                                <p>An automatic subscription for new users has been enabled</p>
                            </div>
                        </div>
                    {/if}
                    {securitytoken}
                    <input type="submit" class="btn btn-success btn-sm row-margin" value="Submit">
                </form>
            {/if}
        </div>
    </div>
</div>

