<div class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="p6" style="margin-bottom: 15px;">
                <p>Mass customer exports are performed in the background using a queue</p>
                <p>The export time depends on the number of exported clients</p>
                <p>You can follow the progress of exporting in <a href="?cmd=queue" target="_blank"> task queue log</a></p>
                <p>If the <strong>Automatic subscription</strong> field is selected in the SendGrid plugin configuration, then it exports <strong>all</strong> clients</p>
                <p>In case the <strong>Automatic subscription</strong> field is disabled, then the export of clients will depend on the defined field <strong>Field for opt-in signup</strong> (The customer will be exported if he agreed to it by marking the defined field)</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-center" style="margin-bottom: 15px;">
            <form action="?cmd={$modulename}&action=mass_export" method="post">
                <input type="submit" name="mass_export" value="Export" class="btn btn-success btn-sm">
                {securitytoken}
            </form>
        </div>
    </div>
</div>