$(function () {
    loadConfig();

    $('.paginercontainer').show();
})

function loadConfig() {
    var id = $('#default_lists').val();
    var security_token = $('input[name="security_token"]').val();
    var auto_sub = $('#auto_sub').val();

    if (auto_sub != 'true'){
        loadMerge(id, security_token);
        loadInterests(id, security_token);
        loadStores(id, security_token);
    }
}

function loadInterests(id, security_token) {
    $.post('?cmd=mailchimp',{
        make: 'loadInterests',
        list_id: id,
        security_token: security_token
    },function(data){
        $('#config_contetnt2').html(data);
    });
}

function customSelect(sel) {
    var data = $(sel).val();
    var id = $(sel).data('id');

    if (data == 'custom'){
        $('#custom'+id).show();
    }else{
        $('#custom'+id).hide();
    }
}

var $loading = $('#loadingDiv').hide();
$(document)
    .ajaxStart(function () {
        $loading.show();
    })
    .ajaxStop(function () {
        $loading.hide();
    });

$("#checkall").change(function() {
    var checkbox = $('.checkbox-export');
    if(this.checked) {
        checkbox.prop('checked', true);
        checkbox.parent('td').parent('tr').addClass('checkedRow');
    }else{
        checkbox.prop('checked', false);
        checkbox.parent('td').parent('tr').removeClass('checkedRow');
    }
});

$('.checkbox-export').change(function () {
   if(this.checked){
       $(this).parent('td').parent('tr').addClass('checkedRow');
   }else{
       $(this).parent('td').parent('tr').removeClass('checkedRow');
   }
});




