<?php
/**
 * Created by PhpStorm.
 * User: Grzesiek
 * Date: 08.02.2018
 * Time: 09:33
 */

class sendgrid_new_controller extends HBController{

    use \Components\Traits\LoggerTrait;
    /**
     * @var nesdgrid_new
     */
    var $module;

    /**
     * Admin authorization object
     * Use $this->authorization->get_id() - to get id of logged in staff member
     * @var AdminAuthorization
     */
    var $authorization;

    /**
     * Template object (subclass of Smarty).
     * Use it to assign variables to template
     * @var Smarty $template
     */
    var $template;


    /**
     * This method is runned ALWAYS before any other method in this class.
     * Use it to initialize variables, assign some common stuff to template.
     * @param $params
     */
    public function beforeCall($params) {
        $modDir = strtolower($this->module->getModuleDirName());
        $this->template->pageTitle = $this->module->getModName();
        $this->template->module_template_dir = APPDIR_MODULES . 'Other' . DS . $modDir . DS . 'admin';
        $this->template->assign('moduleurl', Utilities::checkSecureURL(HBConfig::getConfig('InstallURL') . 'includes/modules/Other/' . $modDir . '/admin/'));
        $this->template->assign('modulename', $this->module->getModuleName());
        $this->template->assign('modname', $this->module->getModName());
        $this->template->assign('moduleid', $this->module->getModuleId());

        $this->template->showtpl = 'tpl/default';
    }
    /**
     * This method is called if no action param is present in URL, or action handler is not found in this class.
     * @param array $params
     */
    public function _default($params) {



        if(isset($params['new_list_name']) AND $params['new_list_name'])
        {
            $result = $this->module->createLists($params['new_list_name']);

            if(!$result){
                $this->template->assign('list_creating_error', 'List name is a duplicate of an existing list or is not a string.');
            }
        }

        if (isset($params['default_lists']) && $params['token_valid']) {
            HBConfig::setConfig('SendGridList', $params['default_lists'], true);
        }

        $lists = $this->module->getLists();

        if (!$lists){
            $this->template->assign('lists_error', 'Unknown error');
        }else{
            $list = HBConfig::getConfig("SendGridList");

            foreach($lists['lists'] AS $k=>$value){

                    if($value['id'] == $list )
                        $lists['lists'][$k]['default'] = 1;
                    else
                        $lists['lists'][$k]['default'] = 0;
            }


            $this->template->assign('lists', $lists['lists']);
        }
        $this->template->assign('tab', 'configuration');
    }

    public function export($params){


        if ($params['token_valid'] && isset($params['save'])){
            if(Components\HBQueue\HBQueue::isEnabled()){

                Components\HBQueue\HBQueue::dispatch([$this->module,"addNotexportedClientsToList"],$params['export'], $params['consent']);
                Engine::addInfo('Exporting clients to the SendGrid list takes place in the background and can take some moment');
            }else{
                $this->logger()->error('HBQueue is disabled -> export client to SendGrid list');
            }
        }


        $clients = $this->module->getNotexportedClients();

        $this->template->assign('clients', $clients);
        $this->template->assign('tab', 'export');
        $this->template->assign('contact', $this->module->_getConfiguration('Export customer contacts'));
        $this->template->assign('modulename', $this->module->getModuleName());
        $this->template->assign('currentpage', Registrator::paranoid(isset($params['page']) ? $params['page'] : 0));
        $this->template->assign('currentfilter', $this->module->getCurrentFilter());
        $this->template->assign('orderby', $this->module->sorter->getCurrentOrder());
        $this->template->assign($this->template->sorterUpdate($this->module->getTotalPages()));
    }

    public function mass_export($params){


        if ($params['token_valid'] && isset($params['mass_export'])){

            $export = $this->module->getAllClients();

            if ($export){
                Engine::addInfo('Clients export are performed in a queue');
            }
        }

        $this->template->assign('tab', 'mass_export');
    }

}